# Blacklist
Contains lists of domains and IPs that are unwanted or possibly dangerous.

## Domains
Lists of domains that are known to be or are potentially dangerous in some way or are
otherwise high risk/unwanted. Subdomains are generally removed and the entire parent
domain is blacklisted except for whitelisted parent domains. Subdomains of whitelisted
domains are still blacklisted individually.
Examples of whitelisted parent domains include:
- blogspot.com
- azurewebsites.net
- cloudapp.net
- cloudfront.net
- blob.core.windows.net
- table.core.windows.net

### Advertising / Tracking
List of domains known to host advertising and tracking. High privacy risk. Possible
security risk.

### Blacklist
This general list contains many domains that are known to have been used at some point to
host malware, points to a CnC server, scams, used as open relays for spam/fraudulent
email, phishing attempts, are compromised in some way, or are otherwise dangerous but
unsorted. High security risk.

### DDNS
Contains domains known to be used by ddns services. DDNS is often used by bad actors to
temporarily host malware. Security risk.

### Free Subdomains
Often used to host malware and scams. Security risk.

### Free Webhosts
Often used to host malware and scams. Security risk.

### Gambling
If you want to gamble you can use your own network. Possible privacy and security risk.

## IPs
Lists of IPs known to be attempting to gain access to SSH honeypots.
